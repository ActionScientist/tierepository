﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameManager.Instance().EndGame();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
