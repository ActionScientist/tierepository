﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public int treasureScore = 0, minionParts = 0, overallScore = 0;
    public bool died = false;

    public GameObject gratz, darn;

    private static GameManager instance = null;


    public static GameManager Instance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
        UpdateGUI();
    }

    public void UpdateGUI()
    {
        foreach (Transform child in transform)
        {
            if (child.name == "ScrapNum")
            {
                child.GetComponent<Text>().text = minionParts.ToString();
            }
            else if (child.name == "ArtifctNum")
            {
                child.GetComponent<Text>().text = treasureScore.ToString();
            }
            else if (child.name == "ArtifctNum")
            {
                child.GetComponent<Text>().text = overallScore.ToString();
            }
            
        }
    }
    public void EndGame()
    {
        foreach(Transform child in transform)
        {
            if (child.gameObject.active)
            {
                child.gameObject.SetActive(false);
            }
            if(!child.gameObject.active)
            {
                child.gameObject.SetActive(true);
            }
        }
        if (died)
        {
            gratz.SetActive(false);
            darn.SetActive(true);
        }
        else
        {
            gratz.SetActive(true);
            darn.SetActive(false);
        }
                
        }
}
