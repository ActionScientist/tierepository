﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthPlayer : MonoBehaviour {

    public int MaxHealth = 200, CurrentHealth = 200;
    public GameObject HealthBar;
    public GameObject HealthSlider, WorldCanvas;
    public float offsetX, offsetY, offsetZ;
    Vector3 BarLoc;

    void Start()
    {
        WorldCanvas = GameObject.Find("WorldCanvas");
        BarLoc = new Vector3(transform.position.x + offsetX, transform.position.y + offsetY, transform.position.z + offsetZ);
        GameObject tempthing;
        tempthing = Instantiate(HealthSlider, BarLoc, Quaternion.Euler(90, 0, 0)) as GameObject;
        tempthing.transform.SetParent(WorldCanvas.transform);
        tempthing.GetComponent<BarFollow>().Parent = this.gameObject;
        tempthing.GetComponent<BarFollow>().offsetX = offsetX;
        tempthing.GetComponent<BarFollow>().offsetY = offsetY;
        tempthing.GetComponent<BarFollow>().offsetZ = offsetZ;
        HealthBar = tempthing;
        UpdateHUD();
    }

    void Update()
    {
        if (CurrentHealth <= 0)
        {
            GameManager.Instance().died = true;
            Application.LoadLevel("End");
        }
    }

    public void Hit(int Damage)
    {
        CurrentHealth -= Damage;
        UpdateHUD();
    }

    void UpdateHUD()
    {
        HealthBar.GetComponent<Slider>().value = CurrentHealth;
    }    
}
