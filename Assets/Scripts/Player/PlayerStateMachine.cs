﻿using UnityEngine;
using System.Collections;

public class PlayerStateMachine : MonoBehaviour 
{
	
	Animator anim;


	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator> ();
		anim.SetBool ("playerLeft", false);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		float move = transform.parent.GetComponent<Rigidbody>().velocity.x;
		float moveV = transform.parent.GetComponent<Rigidbody>().velocity.z;

		anim.SetFloat ("playerHSpeed", move);
		anim.SetFloat ("playerVSpeed", moveV);



		if (move > 0.5f)
			anim.SetBool ("playerLeft", false);
		else if (move < -0.5f)
			anim.SetBool ("playerLeft", true);
	
	}




}

