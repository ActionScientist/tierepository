﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{

    public float WalkSpeed;
    public float WalkCap;
    public float GroundValue = -1.2f;
    public Rigidbody theBullet;
    public bool isLeft = false;
    float vertical;
    float horizontal;
    private Rigidbody rigidbody;

    void Start()
    {
        isLeft = false;
        rigidbody = GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");
    }
    void Update()
    {
        if (GetComponent<Rigidbody>().velocity.x > 0.5)
            isLeft = false;
        else if (GetComponent<Rigidbody>().velocity.x < -0.5)
            isLeft = true;

        if (rigidbody.velocity.magnitude > WalkCap)
            rigidbody.velocity = rigidbody.velocity.normalized * WalkCap;
        else
            rigidbody.AddRelativeForce(new Vector3(horizontal * WalkSpeed, 0f, vertical * WalkSpeed));
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Rigidbody bullet;
            bullet = Instantiate(theBullet, transform.position, transform.rotation) as Rigidbody;
            if (isLeft == true)
                bullet.velocity = transform.TransformDirection(Vector3.left * 20);
            else if (isLeft == false)
                bullet.velocity = transform.TransformDirection(Vector3.right * 20);
        }
    }
}