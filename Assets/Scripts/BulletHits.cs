﻿using UnityEngine;
using System.Collections;

public class BulletHits : MonoBehaviour {

	public AudioClip bswall, bsshoot;
	public GameObject bhit;

	void Start () {
		GetComponent<AudioSource>().PlayOneShot(bsshoot, 0.6F);
	}
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Env"){
			Destroy(this.gameObject);
			AudioSource.PlayClipAtPoint (bswall, transform.position);
		}
        Instantiate(bhit, transform.position, transform.rotation);
    }
}
