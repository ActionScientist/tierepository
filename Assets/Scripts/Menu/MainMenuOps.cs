﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuOps : MonoBehaviour {

    public Transform titleBox, titleThis, titleExplore, startBtn, credBtn;
    float timer = 0f;

	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer >= 1)
        {
            titleBox.gameObject.SetActive(true);
        }
        if (timer >= 2)
        {
            titleThis.gameObject.SetActive(true);
        }
        if (timer >= 3)
        {
            titleExplore.gameObject.SetActive(true);
        }
        if (timer >= 5)
        {
            startBtn.gameObject.SetActive(true);
            credBtn.gameObject.SetActive(true);
        }
    }

    public void StartGame()
    {
        Application.LoadLevel(1);
    }
    public void CreditsMenu()
    {

    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
