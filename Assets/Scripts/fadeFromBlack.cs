﻿using UnityEngine;
using System.Collections;

public class fadeFromBlack : MonoBehaviour {

	Vector3 zeroPos = Vector3.zero;

	void Start (){
		transform.position = zeroPos;
	}
	
	void Update () {
	
		Color textureColor = GetComponent<GUITexture>().color;
		textureColor.a -= Time.deltaTime / 6;
		GetComponent<GUITexture>().color = textureColor;

	}

}
