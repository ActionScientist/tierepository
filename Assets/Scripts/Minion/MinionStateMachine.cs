﻿using UnityEngine;
using System.Collections;

public class MinionStateMachine : MonoBehaviour 
{
	
	Animator anim;
	bool goAttack;

	void Start () {

		anim = GetComponent<Animator> ();
		anim.SetBool ("minionLeft", false);

		goAttack = transform.parent.GetComponent <MinionMove> ().attacking;
	}

	void FixedUpdate ()
    { 
		float move = transform.parent.GetComponent<NavMeshAgent>().velocity.x;
		anim.SetFloat ("minionSpeed", move);

		goAttack = transform.parent.GetComponent <MinionMove>().attacking;
		anim.SetBool ("minionAttack", goAttack);

		if (move > 0)
			anim.SetBool ("minionLeft", false);
		else if (move < 0)
			anim.SetBool ("minionLeft", true);
	}
}

