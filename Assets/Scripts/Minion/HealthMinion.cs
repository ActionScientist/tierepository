﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthMinion : MonoBehaviour {

    public int MaxHealth = 50, CurrentHealth = 50;
    public GameObject HealthBar;
    int Damage = 5;

    public GameObject HealthSlider, WorldCanvas;
    public float offsetX, offsetY, offsetZ = -1f;
    Vector3 BarLoc;

    public GameObject deadMe, scrapPile;
    public int scrapnumber;

    void Start()
    {
        WorldCanvas = GameObject.Find("WorldCanvas");
        BarLoc = new Vector3(transform.position.x + offsetX, transform.position.y + offsetY, transform.position.z + offsetZ);
        GameObject tempthing;
        tempthing = Instantiate(HealthSlider, BarLoc, Quaternion.Euler(90, 0, 0)) as GameObject;
        tempthing.transform.SetParent(WorldCanvas.transform);
        tempthing.GetComponent<BarFollow>().Parent = this.gameObject;
        tempthing.GetComponent<BarFollow>().offsetX = offsetX;
        tempthing.GetComponent<BarFollow>().offsetY = offsetY;
        tempthing.GetComponent<BarFollow>().offsetZ = offsetZ;
        HealthBar = tempthing;
        UpdateHUD();
    }
    void Hit()
    {
        CurrentHealth -= Damage;
        UpdateHUD();
        if (CurrentHealth <= 0)
        {
            Instantiate(deadMe, transform.position, transform.rotation);
            DropScrap();
            Destroy(gameObject);
        }
    }
    void UpdateHUD()
    {
        HealthBar.GetComponent<Slider>().value = CurrentHealth;
    }
    void DropScrap()
    {
        for (int i = 0; i < scrapnumber; i++)
        {
            float tempX = Random.Range(transform.position.x - 1, transform.position.x + 1);
            float tempZ = Random.Range(transform.position.z - 1, transform.position.z + 1);
            Instantiate(scrapPile, new Vector3(tempX, transform.position.y, tempZ + 1), transform.rotation);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            InvokeRepeating("Hit", 0f, 2.5f);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            CancelInvoke("Hit");
        }
    }
    void OnDestroy()
    {
        Destroy(HealthBar);
    }
}