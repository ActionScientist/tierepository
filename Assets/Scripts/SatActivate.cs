﻿using UnityEngine;
using System.Collections;

public class SatActivate : MonoBehaviour {

	public GameObject minion;
    

    void Start()
    {

    }
	void Update () {

		if (GameManager.Instance().minionParts < 4)
			CancelInvoke();
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			if (GameManager.Instance().minionParts >= 4)
			InvokeRepeating ("SpawnMinion", 0.5f, 0.2f);
		}
	}

	void OnTriggerExit(Collider other){
		if(other.tag == "Player"){
			CancelInvoke();
		}
	}

	void SpawnMinion ()
	{
		float tempX = Random.Range (transform.position.x - 5, transform.position.x + 5);
		float tempZ = Random.Range (transform.position.z - 5, transform.position.z + 5);

		Instantiate (minion, new Vector3(tempX, transform.position.y, tempZ), transform.rotation);
        GameManager.Instance().minionParts -= 4;
        GameManager.Instance().UpdateGUI();

    }
}
