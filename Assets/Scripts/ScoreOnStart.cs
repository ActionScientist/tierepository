﻿using UnityEngine;
using System.Collections;

public class ScoreOnStart : MonoBehaviour {

	public int addScore = 0, addParts = 0, addTreasure = 0;

	// Use this for initialization
	void Start () {
        GameManager.Instance().minionParts += addParts;
        GameManager.Instance().treasureScore += addTreasure;
        GameManager.Instance().overallScore += addScore;
        GameManager.Instance().UpdateGUI();

    }
}
