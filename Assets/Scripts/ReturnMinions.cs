﻿using UnityEngine;
using System.Collections;

public class ReturnMinions : MonoBehaviour {

	public GameObject beammeup;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Minion")
		{
			Instantiate(beammeup, new Vector3(other.transform.position.x, 5, other.transform.position.z), other.transform.rotation);

			Destroy(other.gameObject);
		}

		if (other.gameObject.tag == "MCorpse") {
						GameObject[] names = GameObject.FindGameObjectsWithTag ("MCorpse");

						foreach (GameObject item in names) {
			
								Destroy (item);
			
						}
				}
}
}