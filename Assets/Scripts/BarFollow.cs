﻿using UnityEngine;
using System.Collections;

public class BarFollow : MonoBehaviour {
    public GameObject Parent;
    public float offsetX = 0, offsetY = 0, offsetZ = 0;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(Parent.transform.position.x + offsetX, Parent.transform.position.y + offsetY, Parent.transform.position.z + offsetZ) ; 
	}
}
