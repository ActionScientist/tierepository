﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthEnemy : MonoBehaviour {
    public int DamagePlayer = 2, DamageMin = 5;
    int damage = 5;
    public int currentHealth = 50;
    public GameObject HealthBar, WorldCanvas;

    GameObject player;

    public GameObject HealthSlider;
    public float offsetX, offsetY, offsetZ = -1;
    Vector3 BarLoc;

    public AudioClip hitbybullet, eattack;

    public GameObject deadMe, scrapPile;
    public int scrapnumber;

    void Start ()
    {
        WorldCanvas = GameObject.Find("WorldCanvas");
        BarLoc = new Vector3(transform.position.x + offsetX, transform.position.y + offsetY, transform.position.z + offsetZ);
        GameObject tempthing;
        tempthing = Instantiate(HealthSlider, BarLoc, Quaternion.Euler(90,0,0)) as GameObject;
        tempthing.transform.SetParent(WorldCanvas.transform);
        tempthing.GetComponent<BarFollow>().Parent = this.gameObject;
        tempthing.GetComponent<BarFollow>().offsetX = offsetX;
        tempthing.GetComponent<BarFollow>().offsetY = offsetY;
        tempthing.GetComponent<BarFollow>().offsetZ = offsetZ;
        HealthBar = tempthing;
        UpdateHUD();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player = other.gameObject;
            InvokeRepeating("DamagePly", 0f, 2.5f);
        }
        else if (other.tag == "Minion")
        {
            AudioSource.PlayClipAtPoint(eattack, transform.position);
            InvokeRepeating("Hit", 0f, 2.5f);
            print("Minion hit");
            Hit();
        }
        else if (other.tag == "Bullet")
        {
            Destroy(other.gameObject);
            AudioSource.PlayClipAtPoint(hitbybullet, transform.position);
            Hit();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            player = other.gameObject;
            CancelInvoke("DamagePly");
        }
        else if (other.tag == "Minion")
        {
            CancelInvoke("Hit");
        }
    }
    void DamagePly()
    {
        player.GetComponent<HealthPlayer>().Hit(DamagePlayer);
    }

    void Hit()
    {
        print("hit");
        currentHealth -= damage;
        UpdateHUD();
        if (currentHealth <= 0)
        {
            Instantiate(deadMe, transform.position, transform.rotation);
            DropScrap();
            Destroy(gameObject);
        }
    }

    void UpdateHUD()
    {
        HealthBar.GetComponent<Slider>().value = currentHealth;
    }
    void DropScrap()
    {
        for (int i = 0; i<scrapnumber; i++)
        {
			float tempX = Random.Range(transform.position.x - 1, transform.position.x + 1);
            float tempZ = Random.Range(transform.position.z - 1, transform.position.z + 1);
            Instantiate(scrapPile, new Vector3(tempX, transform.position.y, tempZ), transform.rotation);
        }
    }
    void OnDestroy()
    {
        Destroy(HealthBar);
    }
}
