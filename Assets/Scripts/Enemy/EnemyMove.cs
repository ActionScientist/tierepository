﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {
    private GameObject targetPlayer;
    private NavMeshAgent Navagent;
    private bool isStop = false;


    public GameObject Corpse;

    void Start()
    {
        Navagent = GetComponent<NavMeshAgent>();
        this.gameObject.GetComponent<NavMeshAgent>().updateRotation = false;
        BoxCollider boxTrig = gameObject.GetComponent<BoxCollider>();
        boxTrig.isTrigger = true;
    }

    void Update()
    {
        targetPlayer = GameObject.FindWithTag("Player");
        if (!isStop)
        {
            if (Vector3.Distance(transform.position, targetPlayer.transform.position) < 10)
                Navagent.SetDestination(GameObject.FindGameObjectWithTag("Player").transform.position);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Minion")
        {
            this.gameObject.GetComponent<NavMeshAgent>().SetDestination(transform.position);
        }
    }


    GameObject FindClosestEnemy()
    {
        GameObject[] target;
        target = GameObject.FindGameObjectsWithTag("Minion");
        GameObject closest = gameObject;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject element in target)
        {
            Vector3 diff = element.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = element;
                distance = curDistance;
            }
        }
        return closest;
    }
}