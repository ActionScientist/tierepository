﻿using UnityEngine;
using System.Collections;

public class EnemyTwoStageStateMachine : MonoBehaviour 
{
	
	Animator anim;
	bool isMoving;

	// Use this for initialization
	void Start () {

		isMoving = transform.parent.GetComponent<EnemySlimeBall> ().Movement;
		anim = GetComponent<Animator> ();
		anim.SetBool ("Movin", false);

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		isMoving = transform.parent.GetComponent<EnemySlimeBall> ().Movement;

		anim.SetBool ("Movin", isMoving);



	
	}
	


}

