﻿using UnityEngine;
using System.Collections;

public class bulletStateMachine : MonoBehaviour {

	Animator anim;
	bool gLeft = true;
	void Start () {
		gLeft = GameObject.Find("Player_Main").GetComponent<PlayerMove> ().isLeft;
		anim = GetComponent<Animator> ();
		anim.SetBool("goLeft", gLeft);
	}
}
