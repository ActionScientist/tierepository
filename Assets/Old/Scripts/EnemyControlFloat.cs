﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
	
public class EnemyControlFloat : MonoBehaviour 
{
	private GameObject targetPlayer;

	public bool Awake = false;

	private bool attackingPlayer = false;

	private NavMeshAgent Navagent;

	private float awakeTime = 0f;

	public bool Movement;

	public int aggroRange = 10;


	void Start () 
	{
		Movement = false;
		Navagent = GetComponent<NavMeshAgent>();
		this.gameObject.GetComponent<NavMeshAgent>().updateRotation = false;
	}

	void Update () 
	{


		targetPlayer = GameObject.FindWithTag ("Player");
		
		if (Vector3.Distance (transform.position, targetPlayer.transform.position) < aggroRange) 
		{
			if (!attackingPlayer)
			{
				awakeTime += Time.deltaTime;
				Awake = true;

				if (awakeTime >= 0.9f)
				{
				attackingPlayer = true;
				}

			}

			if (attackingPlayer)
			{
				Movement = true;
				Navagent.SetDestination (GameObject.FindGameObjectWithTag ("Player").transform.position);
			}

		}



	}

}