﻿using UnityEngine;
using System.Collections;

public class EnemyFourStageStateMachine : MonoBehaviour 
{
	
	Animator anim;


	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator> ();
		anim.SetBool ("eLeft", false);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		float move = transform.parent.GetComponent<NavMeshAgent>().velocity.x;

		anim.SetFloat ("eSpeed", move);



		if (move > 0)
			anim.SetBool ("eLeft", false);
		else if (move < 0)
			anim.SetBool ("eLeft", true);
	
	}
	


}

