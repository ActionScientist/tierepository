﻿using UnityEngine;
using System.Collections;

public class EnemyFloatStateMachine : MonoBehaviour 
{
	
	Animator anim;
	
	bool comeAlive;
	bool isMoving;
	
	
	// Use this for initialization
	void Start () {
		
		anim = GetComponent<Animator> ();
		comeAlive = transform.parent.GetComponent<EnemyControlFloat>().Awake;
		isMoving = transform.parent.GetComponent < EnemyControlFloat> ().Movement;
		anim = GetComponent<Animator> ();
		anim.SetBool ("Movin", false);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		comeAlive = transform.parent.GetComponent<EnemyControlFloat>().Awake;
		;
		
		anim.SetBool ("Awake", comeAlive);


		isMoving = transform.parent.GetComponent < EnemyControlFloat> ().Movement;
		
		anim.SetBool ("Movin", isMoving);
		
	}
	
	
	
}

