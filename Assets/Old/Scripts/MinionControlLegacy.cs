﻿using UnityEngine;
using System.Collections;

public class MinionControlLegacy : MonoBehaviour {
	//setup states for the minions
	//state 1 is stop, state 2 is move to player, state 0 is move to click
	public int state = 0, smooth, speed = 8, runSpeed = 8;
	Vector3 targetPosition;

	void Start () 
	{
		this.gameObject.GetComponent<NavMeshAgent>().updateRotation = false;

		Vector3 targetPoint = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position; 
		this.gameObject.GetComponent<NavMeshAgent>().SetDestination(targetPoint);
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (Input.GetKeyDown(KeyCode.Mouse2))
		{
			state = 1;
		}
		if (Input.GetKeyDown(KeyCode.Mouse1))
		{
			state = 2;
		}
		if (Input.GetKeyDown(KeyCode.Mouse0))
		{
			state = 0;
		}
		if (state == 0)
		{
			if(Input.GetKey(KeyCode.Mouse0))
			{
				Plane playerPlane = new Plane(Vector3.up, transform.position);
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				float hitdist = 100.0f;
				if (Physics.Raycast (ray, hitdist)) {
					hitdist = 0.0f;
					if (playerPlane.Raycast (ray, out hitdist)) {
						targetPosition = ray.GetPoint(hitdist);
					}
				}
			}
			this.gameObject.GetComponent<NavMeshAgent>().SetDestination(targetPosition);
			//transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);
		}
		if (state == 1)
		{
			//rigidbody.velocity = new Vector3(0,0,0);
			this.gameObject.GetComponent<NavMeshAgent>().SetDestination(transform.position);
		}
		if (state == 2)
		{
			Vector3 targetPoint = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position; 
			this.gameObject.GetComponent<NavMeshAgent>().SetDestination(targetPoint);
			//transform.position = Vector3.MoveTowards(transform.position, targetPoint, Time.deltaTime * speed);
		}
	}
}
