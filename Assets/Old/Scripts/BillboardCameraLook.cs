﻿using UnityEngine;
using System.Collections;

public class BillboardCameraLook : MonoBehaviour
{
	public Camera m_Camera;
	
	void Update()
	{
		
		Vector3 ThisToCamera = m_Camera.transform.position - this.transform.position;
		
		this.transform.LookAt (this.transform.position - ThisToCamera);
		
		
	}
}