﻿using UnityEngine;
using System.Collections;

public class AnimTimerSpawnScr : MonoBehaviour {

	public GameObject minion;
	public float timer;
	public float timerlimit;
	// Use this for initialization
	void Start () {

		timerlimit = 1.5f;
	
	}
	
	// Update is called once per frame
	void Update () {

			timer += Time.deltaTime;

			
			if (timer >= timerlimit)
			{
			Instantiate (minion, transform.position, transform.rotation);
			Destroy (this.gameObject);
			}
			
		}

	}
