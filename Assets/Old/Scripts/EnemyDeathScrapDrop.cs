﻿using UnityEngine;
using System.Collections;

public class EnemyDeathScrapDrop : MonoBehaviour {
	public Transform scrapPile;
	public int scrapnumber;
	// Use this for initialization
	void OnDestroy()
	{
		for (int i = 0; i < scrapnumber; i++) {
			float tempX = Random.Range (transform.position.x - 1, transform.position.x + 1);
			float tempZ = Random.Range (transform.position.z - 1, transform.position.z + 1);

			Transform theScrap;
			theScrap = Instantiate (scrapPile, new Vector3(tempX, transform.position.y, tempZ), transform.rotation) as Transform;
			theScrap.GetComponent<Rigidbody>().AddTorque(Vector3.up* 50);
			}
	}
}
