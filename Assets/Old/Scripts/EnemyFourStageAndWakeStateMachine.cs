﻿using UnityEngine;
using System.Collections;

public class EnemyFourStageAndWakeStateMachine : MonoBehaviour 
{
	
	Animator anim;

	bool comeAlive;


	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator> ();
		anim.SetBool ("eLeft", false);
		comeAlive = transform.parent.GetComponent<EnemyControlFourwithWake>().Awake;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		float move = transform.parent.GetComponent<NavMeshAgent>().velocity.x;
		comeAlive = transform.parent.GetComponent<EnemyControlFourwithWake>().Awake;

		anim.SetFloat ("eSpeed", move);

		anim.SetBool ("eAwake", comeAlive);



		if (move > 0)
			anim.SetBool ("eLeft", false);
		else if (move < 0)
			anim.SetBool ("eLeft", true);
	
	}
	


}

