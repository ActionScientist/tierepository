﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

	public int numMin = 0, BarHeight = 10;
	public float trigSize = 2.0f, health = 100f, timeToHit = 0.25f, yUpDown = -57.68f, xRightLeft = -48.44f;
	public Texture2D HpBarTexture, FullBarTexture;
	public Camera MainCam;
	public float BaseDamagetoMinion = 2f, BaseDamagetoSelf = 2f;
	Vector3 worldPosition, screenPosition = new Vector3();
	float hpBarLength, fullBarLength;
	//public ArrayList minInTrig = new ArrayList();
	private GameObject targetPlayer;
	private NavMeshAgent Navagent;
	private bool isInTrig = false, isStop = false, isPlayer = false;
	public AudioClip hitbybullet, eattack;

    public GameObject Corpse;
    // Use this for initialization

    void Start () 
	{
		Navagent = GetComponent<NavMeshAgent>();
		this.gameObject.GetComponent<NavMeshAgent>().updateRotation = false;
		fullBarLength = health;
		InvokeRepeating("HealthDecrement", timeToHit, timeToHit);
		BoxCollider boxTrig = gameObject.GetComponent<BoxCollider>();
		boxTrig.isTrigger = true;
		boxTrig.size = new Vector3(trigSize,trigSize,trigSize);
		MainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}

	void Update ()
	{
		hpBarLength = health;
		if (health <= 0.0f)
		{
			if (isPlayer)
				GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().numEnemies -= 1;
			Destroy(this.gameObject);
            Instantiate(Corpse, transform.position, transform.rotation);
            //add death animation here
        }
		targetPlayer = GameObject.FindWithTag ("Player");
		if (!isStop)
		{
			if (Vector3.Distance (transform.position, targetPlayer.transform.position) < 10)
				Navagent.SetDestination(GameObject.FindGameObjectWithTag("Player").transform.position);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Minion")
		{
			numMin += 1;
			this.gameObject.GetComponent<NavMeshAgent>().SetDestination(transform.position);
			InvokeRepeating("AttackClosestEnemy", timeToHit, timeToHit);
			//AddToArray(other.gameObject);
		}
		if (other.gameObject.tag == "PlayerBullet")
		{
			Destroy(other.gameObject);
			AudioSource.PlayClipAtPoint (hitbybullet, transform.position);
			health -= 5;
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "Minion")
		{
			numMin -= 1;
			isStop = false;
			isInTrig = false;
		}
		if (other.gameObject.tag == "Player")
			isPlayer = false;
	}

	void OnTriggerStay (Collider other)
	{
		if (other.gameObject.tag == "Minion")
		{
			isInTrig = true;
			isStop = true;
		}
		if (other.gameObject.tag == "Player")
			isPlayer = true;
	}

	void HealthDecrement()
	{
		health -= BaseDamagetoMinion * (numMin * timeToHit);
		if (numMin > 0)
			GetComponent<AudioSource>().PlayOneShot(eattack, 0.7F);
			
	}

	void OnGUI () {
		worldPosition = new Vector3(transform.position.x, transform.position.y,transform.position.z);
		screenPosition = MainCam.GetComponent<Camera>().WorldToScreenPoint(worldPosition);
		if (health > 0)
		{
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, fullBarLength, BarHeight), FullBarTexture);
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, hpBarLength, BarHeight), HpBarTexture); 
		}
	}

	GameObject FindClosestEnemy() 
	{
		GameObject[] target;
		target = GameObject.FindGameObjectsWithTag("Minion");
		GameObject closest = gameObject;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject element in target)
		{
			Vector3 diff = element.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = element;
				distance = curDistance;
			}
		}
		return closest;
	}
	void AttackClosestEnemy()
	{
				GameObject cEnemy = FindClosestEnemy ();
				if (isInTrig) {
						cEnemy.gameObject.GetComponent<MinionControl> ().health -= BaseDamagetoSelf * timeToHit;
				}	
	}
}