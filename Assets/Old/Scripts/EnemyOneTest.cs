﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
	
public class EnemyOneTest : MonoBehaviour 
{
	private GameObject targetPlayer;

	private NavMeshAgent Navagent;

	public int aggroRange = 10;

	void Start () 
	{
		Navagent = GetComponent<NavMeshAgent>();
		this.gameObject.GetComponent<NavMeshAgent>().updateRotation = false;
	}

	void Update () 
	{

		targetPlayer = GameObject.FindWithTag ("Player");
		
		if (Vector3.Distance (transform.position, targetPlayer.transform.position) < aggroRange)
			Navagent.SetDestination(GameObject.FindGameObjectWithTag("Player").transform.position);


	}
}