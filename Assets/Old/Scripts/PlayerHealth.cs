﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {
	public int health = 200, numEnemies = 0, BarHeight = 10;
	public Texture2D HpBarTexture, FullBarTexture;
	public Camera MainCam;
	Vector3 worldPosition, screenPosition = new Vector3();
	float hpBarLength, fullBarLength;
	public float yUpDown = -57.68f, xRightLeft = -48.44f;
	public AudioClip eattack;

	void Start()
	{
		fullBarLength = health / 2;
		InvokeRepeating("PlayerHealthDecrement", 1, 1);
		MainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			numEnemies += 1;
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			numEnemies -= 1;
		}
	}

	void PlayerHealthDecrement()
	{
		health -= numEnemies;
		if (numEnemies > 0)
			GetComponent<AudioSource>().PlayOneShot(eattack, 0.7F);
	}
	void Update ()
	{
		hpBarLength = health/2;
	}
	void OnGUI () 
	{
		worldPosition = new Vector3(transform.position.x, transform.position.y,transform.position.z);
		screenPosition = MainCam.GetComponent<Camera>().WorldToScreenPoint(worldPosition);
		if (health > 0)
		{
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, fullBarLength, BarHeight), FullBarTexture);
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, hpBarLength, BarHeight), HpBarTexture); 
		}
	}
}