﻿using UnityEngine;
using System.Collections;

public class ArtifactCollection : MonoBehaviour {

	public int maxHealth = 200, numMins = 0, BarHeight = 10;
	public Texture2D HpBarTexture, FullBarTexture;
	public Camera MainCam;
	public GameObject teleportinstance;
	Vector3 worldPosition, screenPosition = new Vector3();
	float hpBarLength, fullBarLength;
	public float yUpDown = -57.68f, xRightLeft = -48.44f, timeToHit = 0.25f, health = 0;

	void Start()
	{
		fullBarLength = maxHealth/2;
		InvokeRepeating("TreasureCalculation", timeToHit, timeToHit);
		MainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player" || other.gameObject.tag == "Minion")
		{
			numMins += 1;
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "Player" || other.gameObject.tag == "Minion")
		{
			numMins -= 1;
		}
	}
	
	void TreasureCalculation()
	{
		health += (numMins * 2) * timeToHit;
	}
	void Update ()
	{
		hpBarLength = health/2;
		if (health >= maxHealth)
		{
			Instantiate (teleportinstance, transform.position, transform.rotation);
			GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>().treasureScore += 1;
			Destroy(this.gameObject);
		}
	}
	void OnGUI () 
	{
		worldPosition = new Vector3(transform.position.x, transform.position.y,transform.position.z);
		screenPosition = MainCam.GetComponent<Camera>().WorldToScreenPoint(worldPosition);
		if (health > 0)
		{
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, fullBarLength, BarHeight), FullBarTexture);
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, hpBarLength, BarHeight), HpBarTexture); 
		}
	}
}
