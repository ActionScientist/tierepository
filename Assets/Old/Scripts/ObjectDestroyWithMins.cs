﻿using UnityEngine;
using System.Collections;

public class ObjectDestroyWithMins : MonoBehaviour {

	public int maxHealth = 100, numMins = 0, BarHeight = 10;
	public Texture2D HpBarTexture, FullBarTexture;
	public Camera MainCam;
	Vector3 worldPosition, screenPosition = new Vector3();
	float hpBarLength, fullBarLength;
	public float yUpDown = -57.68f, xRightLeft = -48.44f, timeToHit = 0.25f, health = 100f;
	
	void Start()
	{
		fullBarLength = maxHealth;
		InvokeRepeating("ObjectCalculation", timeToHit, timeToHit);
		MainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player" || other.gameObject.tag == "Minion")
		{
			numMins += 1;
		}
		if (other.gameObject.tag == "PlayerBullet")
		{
			Destroy(other.gameObject);
			health -= 5;
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "Player" || other.gameObject.tag == "Minion")
		{
			numMins -= 1;
		}
	}
	
	void ObjectCalculation()
	{
		health -= (numMins * 2) * timeToHit;
	}
	void Update ()
	{
		hpBarLength = health;
		if (health <= 0f)
		{
			Destroy(this.gameObject);
		}
	}
	void OnGUI () 
	{
		worldPosition = new Vector3(transform.position.x, transform.position.y,transform.position.z);
		screenPosition = MainCam.GetComponent<Camera>().WorldToScreenPoint(worldPosition);
		if (health > 0)
		{
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, fullBarLength, BarHeight), FullBarTexture);
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, hpBarLength, BarHeight), HpBarTexture); 
		}
	}
}
