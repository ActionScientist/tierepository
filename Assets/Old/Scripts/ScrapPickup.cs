﻿using UnityEngine;
using System.Collections;

public class ScrapPickup : MonoBehaviour {

	//public GameObject teleObject;
	//public GameObject theGUITexture;

	void OnTriggerEnter (Collider other)
	{
		//GameObject pointText;
		if (other.gameObject.tag == "Player")
		{
			GameManager.Instance().minionParts += (int) Random.value + 1;
            GameManager.Instance().UpdateGUI();
            /*pointText = Instantiate(theGUITexture, other.transform.position, transform.rotation) as GameObject;
			pointText.GetComponent<TextMesh>().text = "+1";
			pointText.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 250);
			GameObject theCamera;
			theCamera = GameObject.FindWithTag("MainCamera");
			Vector3 oppositeCam = theCamera.transform.position;
			oppositeCam.z = oppositeCam.z * -1;
			oppositeCam.y = oppositeCam.y * -1;
			pointText.gameObject.transform.LookAt(oppositeCam);
			Instantiate (teleObject, transform.position, Quaternion.identity);
			Destroy (pointText.gameObject, 1);*/
            Destroy(this.gameObject);
		}
	}		 
}
