﻿using UnityEngine;
using System.Collections;

public class ArtifactTrigger : MonoBehaviour {
	public GameObject emptySpawn;
	public GameObject[] gamObjUse = new GameObject[6];
	GameObject objUse;
	int counter = 0;
	public int numWaves = 5;
	bool isGo = true;
	void Update()
	{
		if (counter >= numWaves)
			CancelInvoke();
	}
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player" || other.tag == "Minion" && isGo)
		{
			InvokeRepeating("Main", 1, 1);
			isGo = false;
		}
	}
	void Main()
	{
		float ranVar = RandomVariableFind();
		GameObject toUse = UseEnemy(ranVar);
		Instantiate(toUse, emptySpawn.transform.position, emptySpawn.transform.rotation);
		counter++;
	}
	static float RandomVariableFind()
	{
		float num = Random.value;
		return num;
	}
	GameObject UseEnemy(float compRan)
	{
		if (compRan >= 0.0f & compRan < .15f)
			objUse = gamObjUse[0];
		else if (compRan >= .15f && compRan < .35f)
			objUse = gamObjUse[1];
		else if (compRan >= .35f && compRan < .45f)
			objUse = gamObjUse[2];
		else if (compRan >= .45f && compRan < .65f)
			objUse = gamObjUse[3];
		else if (compRan >= .65f && compRan <= .8f)
			objUse = gamObjUse[4];
		else if (compRan >= .8f && compRan <= 1.0f)
			objUse = gamObjUse[5];
		return objUse;
	}
}
