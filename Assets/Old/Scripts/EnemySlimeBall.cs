﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
	
public class EnemySlimeBall : MonoBehaviour 
{
	private GameObject targetPlayer;

	private NavMeshAgent Navagent;

	public bool Movement;

	public int aggroRange = 10;

	void Start () 
	{
		Movement = false;
		Navagent = GetComponent<NavMeshAgent>();
		this.gameObject.GetComponent<NavMeshAgent>().updateRotation = false;
	}

	void Update () 
	{

		targetPlayer = GameObject.FindWithTag ("Player");
		
		if (Vector3.Distance (transform.position, targetPlayer.transform.position) < aggroRange) {
						Movement = true;
						Navagent.SetDestination (GameObject.FindGameObjectWithTag ("Player").transform.position);
				}

	}
}