﻿using UnityEngine;
using System.Collections;

public class MinionControl : MonoBehaviour {
	//setup states for the minions
	//state 1 is stop, state 2 is move to player, state 0 is move to click, state 3 is go to enemy
	public GameObject theGUITexture;
	public int state = 1, BarHeight = 10;
	Vector3 targetPosition;
	Vector3 targetPoint;
	public float health = 100f, yUpDown = 19.98f, xRightLeft = -48.44f;
	public Texture2D HpBarTexture, FullBarTexture;
	public Camera MainCam;
	Vector3 worldPosition, screenPosition = new Vector3();
	float hpBarLength, fullBarLength;
	bool isStop = true;
	public bool attacking = false;

    public GameObject Corpse;

    public AudioClip mfollow, mstop, maggro, mmove, mshock;
	
	void Start()
	{
		fullBarLength = health;
		MainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		state = 2;
	}
	
	void Update () 
	{
		hpBarLength = health;
		this.gameObject.GetComponent<NavMeshAgent>().updateRotation = false;
		if (Input.GetKeyDown(KeyCode.F))
		{
			if (state == 0 || state == 3)
			{
				state = 2;
				GetComponent<AudioSource>().PlayOneShot(mfollow, 0.8F);
			}
			if (state == 1)
			{
				state = 2;
				GetComponent<AudioSource>().PlayOneShot(mfollow, 0.8F);
			}
			else if (state == 2)
				state = 1;
			GetComponent<AudioSource>().PlayOneShot(mstop, 0.8F);
		}
		if (Input.GetKeyDown(KeyCode.Mouse0))
		{
			state = 0;
			isStop = false;
		}
		if (Input.GetKeyUp(KeyCode.Mouse0))
		{
			isStop = true;
		}
		if (Input.GetKeyDown (KeyCode.G)) {
						state = 3;
			GetComponent<AudioSource>().PlayOneShot(maggro, 0.7F);
				}
		if (state == 0)
		{
			if(Input.GetKey(KeyCode.Mouse0))

			{
				GetComponent<AudioSource>().PlayOneShot(mmove, 0.5F);


				Plane playerPlane = new Plane(Vector3.up, transform.position);
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				float hitdist = 150.0f;
				if (Physics.Raycast (ray, hitdist)) 
				{
					hitdist = 0.0f;
					if (playerPlane.Raycast (ray, out hitdist)) 
						targetPosition = ray.GetPoint(hitdist);
				}
			}
			this.gameObject.GetComponent<NavMeshAgent>().SetDestination(targetPosition);
		}
		if (state == 1) {
						this.gameObject.GetComponent<NavMeshAgent> ().SetDestination (transform.position);
				}
		if (state == 2)
		{
			targetPoint = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position; 
			this.gameObject.GetComponent<NavMeshAgent>().SetDestination(targetPoint);
		}
		if (state == 3)
		{
			GameObject cEnemy = FindClosestEnemy();
			targetPoint = cEnemy.transform.position;
			this.gameObject.GetComponent<NavMeshAgent>().SetDestination(targetPoint);
		}
		GameObject pointText;
		if (health <= 0) 
		{
			pointText = Instantiate(theGUITexture, this.transform.position, transform.rotation) as GameObject;
			pointText.GetComponent<TextMesh>().text = "(×_×#";
			pointText.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 250);
			Destroy (pointText.gameObject, 1);
            Instantiate(Corpse, transform.position, transform.rotation);
            Destroy (this.gameObject);
		}
	}
	
	GameObject FindClosestEnemy() 
	{
		GameObject[] target;
		target = GameObject.FindGameObjectsWithTag("Enemy");
		GameObject closest = gameObject;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject element in target)
		{
			Vector3 diff = element.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = element;
				distance = curDistance;
			}
		}
		return closest;
	}
	
	void OnGUI () 
	{
		worldPosition = new Vector3(transform.position.x, transform.position.y,transform.position.z);
		screenPosition = MainCam.GetComponent<Camera>().WorldToScreenPoint(worldPosition);
		if (health > 0)
		{
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, fullBarLength, BarHeight), FullBarTexture);
			GUI.DrawTexture(new Rect(screenPosition.x + xRightLeft, (Screen.height - screenPosition.y) + yUpDown, hpBarLength, BarHeight), HpBarTexture); 
		}
	}
	void OnTriggerStay (Collider other)
	{
		if (other.tag == "Enemy" && isStop == true) {
						state = 1;
						attacking = true;
						GetComponent<AudioSource>().PlayOneShot(mshock, 1F);
				}
		else if (other.tag == "Wall" && isStop == true) {
			state = 1;
			attacking = true;
			GetComponent<AudioSource>().PlayOneShot(mshock, 1F);
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Enemy") {
						state = 3;
						attacking = false;
				}
	}
}