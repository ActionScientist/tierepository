﻿// Click To Move script
// Moves the object towards the mouse position on left mouse click

var smooth:int; // Determines how quickly object moves towards position
var speed : int = 8;
private var targetPosition:Vector3;

function Update () {
    if(Input.GetKey(KeyCode.Mouse0))
    {
        var playerPlane = new Plane(Vector3.up, transform.position);
        var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
        var hitdist = 50.0;
        if (Physics.Raycast (ray, hitdist)) {
        hitdist = 0.0;
	        if (playerPlane.Raycast (ray, hitdist)) {
	            var targetPoint = ray.GetPoint(hitdist);
	            targetPosition = ray.GetPoint(hitdist);
	        }
	       }
    }
    
     transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);
}