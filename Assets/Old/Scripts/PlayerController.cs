﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour 
{
	
	public float WalkSpeed;
	
	public float TurnDampAmount;
	
	public float WalkCap;
	
	public float RunCap;
	
	public float LookDownCap;
	
	public float LookUpCap;
	
	private float RotAmount;
	
	private Vector3 RotAdd;
	
	//private bool IsGrounded;
	
	public float JumpVelocity;
	
	public float MouseLook = .5f;
	
	public bool Rotating = false;
	
	public float GroundValue = -1.2f;
	
	private Vector3 AngularVel;

	public Rigidbody theBullet;
	
	private string DisplayString;

	public bool isLeft = false;



	

	
	void Start () 
	{
		//RotAdd = Vector3.zero;
		
		//Screen.showCursor = false;
		
		//Screen.lockCursor = true;
		
		//AngularVel = Vector3.zero;
		
		//DisplayString = "Inventory: " + "\n\n\n";

		isLeft = false;
		
		
	}
	


	void Update () 
	{
		
		// Check Our Grounded Status
		
		//GetGrounded ();
		
		// Keyboard Movement

		if (GetComponent<Rigidbody>().velocity.x > 0.5)
			isLeft = false;
		else if (GetComponent<Rigidbody>().velocity.x < -0.5)
			isLeft = true;


		if(Input.GetKey(KeyCode.W))
		{
			if(Vector3.Distance(Vector3.zero, this.transform.GetComponent<Rigidbody>().velocity + (this.transform.forward * WalkSpeed * Time.deltaTime)) < WalkCap)
				this.GetComponent<Rigidbody>().velocity += this.transform.forward * WalkSpeed * Time.deltaTime;
		}
		
		if(Input.GetKey(KeyCode.S))
		{
			if(Vector3.Distance(Vector3.zero, this.transform.GetComponent<Rigidbody>().velocity - (this.transform.forward * WalkSpeed * Time.deltaTime)) < WalkCap)
				this.GetComponent<Rigidbody>().velocity += -this.transform.forward * WalkSpeed * Time.deltaTime;
		}
		
		if(Input.GetKey(KeyCode.A))
		{
			if(Vector3.Distance(Vector3.zero, this.transform.GetComponent<Rigidbody>().velocity - (this.transform.right * WalkSpeed * Time.deltaTime)) < WalkCap)
				this.GetComponent<Rigidbody>().velocity += -this.transform.right * WalkSpeed * Time.deltaTime;
		}
		
		if(Input.GetKey(KeyCode.D))
		{
			if(Vector3.Distance(Vector3.zero, this.transform.GetComponent<Rigidbody>().velocity + (this.transform.right * WalkSpeed * Time.deltaTime)) < WalkCap)
				this.GetComponent<Rigidbody>().velocity += this.transform.right * WalkSpeed * Time.deltaTime;
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			Rigidbody bullet;
			bullet = Instantiate(theBullet, GameObject.Find ("space_guyidle_strip4_0").transform.position, transform.rotation) as Rigidbody;

			if (isLeft == true)
				bullet.velocity = transform.TransformDirection(Vector3.left * 20);
			else if (isLeft == false)
				bullet.velocity = transform.TransformDirection(Vector3.right * 20);
		}
		
		/*if(Vector3.Distance(Vector3.zero, this.rigidbody.velocity) > WalkCap)
		{
			//this.rigidbody.velocity -= this.transform.forward * WalkSpeed * Time.deltaTime;
		}*/
		
		
		// Mouse Looking
		/*if(!Rotating)
		{
			RotAdd.x -= Input.GetAxis ("Mouse Y");
			
			Transform camera = GameObject.FindWithTag ("MainCamera").transform;
			
			camera.rotation = Quaternion.Euler (camera.rotation.eulerAngles + RotAdd * MouseLook);
			
			
			RotAdd *= TurnDampAmount;
			
			
			AngularVel += new Vector3(0, Input.GetAxis ("Mouse X"), 0);
			
			AngularVel *= TurnDampAmount;
			
			this.transform.Rotate(AngularVel);
		}
		// Jumping
		
		if(Input.GetKeyDown (KeyBindings.GetBind ("JUMP")) && IsGrounded)
		{
			this.rigidbody.velocity += new Vector3(0,JumpVelocity,0);
		}
		
		// Interaction
		
		if(Input.GetKeyDown (KeyBindings.GetBind ("USE")))
		{
			
		}
		*/
	}
	

	private bool IsBetween(float Min, float Max, float C)
	{
		return (C >= Min && C <= Max) ? true : false;
	}
	
	/*private void GetGrounded()
	{
		RaycastHit hit = new RaycastHit();
		
		Ray castRay = new Ray(this.transform.position, (this.transform.position + new Vector3(0, GroundValue, 0)) - this.transform.position);
		
		IsGrounded = Physics.Raycast(castRay, out hit, 1.2f);
	}
	*/
	
	
	
}
