﻿using UnityEngine;
using System.Collections;

public class ScrapPickupLegacy : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>().minionParts += (int) Random.value + 1;
			Destroy (this.gameObject);
		}
	}
}
