﻿using UnityEngine;
using System.Collections;

public class WinLose : MonoBehaviour {

	public bool dead = false, won = false;
	public GameObject youWin, loseScreen;
	Vector3 zeroPos = Vector3.zero;
	Color textureColor;
	Color nullAlpha;

	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
		dead = false;
		won = false;
	}

	void Start () {
		nullAlpha.r = 128;
		nullAlpha.g = 128;
		nullAlpha.b = 128;
		nullAlpha.a = 0;
	}

	void Update () {

		youWin.transform.position = zeroPos;
		loseScreen.transform.position = zeroPos;

		if(Input.GetKeyDown (KeyCode.Alpha1)){
			dead = true;
		}
		if(Input.GetKeyDown (KeyCode.Alpha2)){
			won = true;
		}
	
		if(dead){
			textureColor = loseScreen.GetComponent<GUITexture>().color;
			textureColor.a += Time.deltaTime / 6;
			loseScreen.GetComponent<GUITexture>().color = textureColor;
			Invoke("GoToEnd", 5);
		}
		if(won){
			textureColor = youWin.GetComponent<GUITexture>().color;
			textureColor.a += Time.deltaTime / 6;
			youWin.GetComponent<GUITexture>().color = textureColor;
			Invoke("GoToEnd", 5);
		}

	}

	void GoToEnd(){
		Application.LoadLevel("End");
	}
	
}
